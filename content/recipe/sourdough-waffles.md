---
title: "Sourdough Waffles"
date: 2020-10-21T20:43:44-04:00
draft: false
tags: ["waffles", "breakfast", "sweets", "sourdough"]
---
## Notes
This recipe is still a work in progress, but works as it is.

## Ingredients
* 275g all-purpose flour
* 1 1/2 cups water
* 1 cup sourdough starter
* 1/2 cup vegetable oil
* 2 large eggs
* 3 tablespoons sugar
* 2 teaspoons salt
* 1 tablespoon water
* 1 teaspoon baking soda

## Directions

### Step 1
Mix flour, water, and sourdough starter in a large bowl to make batter. Cover loosely with plastic wrap and let sit in a warm place until filled with large bubbles, four hours to overnight.

### Step 2
Stir oil, eggs, sugar, and salt into batter; mix well to combine.

### Step 3
Whisk 1 tablespoon water and baking soda together in a small bowl until dissolved. Fold into the batter with rubber spatula.

### Step 4
Preheat waffle iron according to manufacturer's directions.

### Step 5
Add batter to waffle iron and cook until crisp and golden, according to manufacturer's instructions.

