---
title: "Sourdough Pancakes"
date: 2020-10-22T15:54:30-04:00
draft: false
tags: ["pancakes", "breakfast", "sweets", "sourdough"]
---
## Notes
This recipe is old and I am not sure where I got it from. I think from Kaitlyn
Feiock years ago, but don't hold me to that! No matter, this is an amazing
recipe for pancakes you'll come back to constantly.

## Ingredients
* 1 cup sourdough starter
* 1.5 cups all-purpose flour
* 1 cup warm water (85 - 90°F)
* 1 egg (lightly beaten)
* 1 tablespoon sugar
* 1 tablespoon unsalted butter (melted)
* 3/4 teaspoon salt
* 3/4 teaspoon baking soda
* 2 tablespoons milk

## Directions

### Step 1 - The night before
In a large bowl add the sourdough starter and warm water and mix well. Then
slowly incorporate the flour. Cover and let sit over night.

### Step 2 - The next morning
Remove a cup of your new sourdough mix and return it to the refrigerator.

### Step 3 - Pancakes!
In the large bowl containing the remaining 1.5 cups of sourdough starter, 
combine the egg, sugar, butter, salt, baking soda, and milk. Mix well.

Cook on a 400°F griddle or fry in a ban at medium-low heat.
